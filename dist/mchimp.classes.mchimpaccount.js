"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const plugins = require("./mchimp.plugins");
const helpers = require("./mchimp.helpers");
const altRequest = require("request");
// interfaces
class MchimpAccount {
    constructor() {
        // nothing here
    }
    /**
     * authenticate
     */
    auth(keyArg) {
        this._authBase64 = `apikey ${keyArg}`;
        this._datacenter = helpers.getDataCenterFromKey(keyArg);
    }
    /**
     * smartsubscribe to a list
     */
    smartsubscribe(listNameArg, mailAdressArg, statusArg, inputObjectArg, interestArrayArg) {
        return __awaiter(this, void 0, void 0, function* () {
            let list = yield this.getListWithName(listNameArg);
            let interestObject = {};
            let availableInterests = yield this.getInterestsForList(listNameArg);
            for (let interest of interestArrayArg) {
                let validInterest = availableInterests.filter(itemArg => {
                    return itemArg.name === interest;
                });
                if (validInterest) {
                    interestObject[validInterest[0].id] = true;
                }
            }
            let response = yield this.request('POST', `/lists/${list.id}/members`, {
                email_address: mailAdressArg,
                status: statusArg,
                merge_fields: inputObjectArg,
                interests: interestObject
            });
            console.log(response);
        });
    }
    // gets all available lists on an account
    getLists() {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield this.request('GET', '/lists')).lists;
        });
    }
    getListWithName(nameArg) {
        return __awaiter(this, void 0, void 0, function* () {
            let lists = yield this.getLists();
            return lists.filter(itemArg => {
                return itemArg.name === nameArg;
            })[0];
        });
    }
    getInterestsForList(listNameArg) {
        return __awaiter(this, void 0, void 0, function* () {
            let list = yield this.getListWithName(listNameArg);
            let interestsArray = [];
            let interestCategories = (yield this.request('GET', `/lists/${list.id}/interest-categories`)).categories;
            for (let interestCategorie of interestCategories) {
                let interests = (yield this.request('GET', `/lists/${list.id}/interest-categories/${interestCategorie.id}/interests`)).interests;
                interestsArray = interestsArray.concat(interests);
            }
            return interestsArray;
        });
    }
    request(methodArg, routeArg, dataArg = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            let dataString = JSON.stringify(dataArg, null, ' ');
            // console.log(routeArg)
            // console.log(dataString)
            let optionsArg = {
                method: methodArg,
                headers: {
                    'Authorization': this._authBase64,
                    'content-type': 'application/json'
                },
                requestBody: dataString
            };
            let requestRoute = `https://${this._datacenter}.api.mailchimp.com/3.0${routeArg}`;
            let response;
            if (methodArg !== 'POST') {
                response = yield plugins.smartrequest.request(requestRoute, optionsArg);
                return response.body;
            }
            else {
                altRequest.post({ url: requestRoute, body: dataString, headers: {
                        'Authorization': this._authBase64,
                        'content-type': 'application/json'
                    } }, (err, body) => {
                    console.log(body);
                });
            }
        });
    }
}
exports.MchimpAccount = MchimpAccount;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWNoaW1wLmNsYXNzZXMubWNoaW1wYWNjb3VudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3RzL21jaGltcC5jbGFzc2VzLm1jaGltcGFjY291bnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLDRDQUEyQztBQUMzQyw0Q0FBMkM7QUFDM0Msc0NBQXFDO0FBRXJDLGFBQWE7QUFFYjtJQUlFO1FBQ0UsZUFBZTtJQUNqQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxJQUFJLENBQUUsTUFBYztRQUNsQixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsTUFBTSxFQUFFLENBQUE7UUFDckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUE7SUFDekQsQ0FBQztJQUVEOztPQUVHO0lBQ0csY0FBYyxDQUNsQixXQUFtQixFQUNuQixhQUFxQixFQUNyQixTQUFpQixFQUNqQixjQUFtQixFQUNuQixnQkFBMEI7O1lBRTFCLElBQUksSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUNsRCxJQUFJLGNBQWMsR0FBUSxFQUFFLENBQUE7WUFDNUIsSUFBSSxrQkFBa0IsR0FBRyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUNwRSxHQUFHLENBQUMsQ0FBQyxJQUFJLFFBQVEsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLElBQUksYUFBYSxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxPQUFPO29CQUNuRCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUE7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFBO2dCQUNGLEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFBO2dCQUM1QyxDQUFDO1lBQ0gsQ0FBQztZQUNELElBQUksUUFBUSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUMsVUFBVSxJQUFJLENBQUMsRUFBRSxVQUFVLEVBQUU7Z0JBQ3BFLGFBQWEsRUFBRSxhQUFhO2dCQUM1QixNQUFNLEVBQUUsU0FBUztnQkFDakIsWUFBWSxFQUFFLGNBQWM7Z0JBQzVCLFNBQVMsRUFBRSxjQUFjO2FBQzFCLENBQUMsQ0FBQTtZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDdkIsQ0FBQztLQUFBO0lBRUQseUNBQXlDO0lBQ25DLFFBQVE7O1lBQ1osTUFBTSxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtRQUNwRCxDQUFDO0tBQUE7SUFFSyxlQUFlLENBQUUsT0FBZTs7WUFDcEMsSUFBSSxLQUFLLEdBQVUsTUFBTSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7WUFDeEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTztnQkFDekIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFBO1lBQ2pDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ1AsQ0FBQztLQUFBO0lBRUssbUJBQW1CLENBQUUsV0FBbUI7O1lBQzVDLElBQUksSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUNsRCxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUE7WUFDdkIsSUFBSSxrQkFBa0IsR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsVUFBVSxJQUFJLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFBO1lBQ3hHLEdBQUcsQ0FBQyxDQUFDLElBQUksaUJBQWlCLElBQUksa0JBQWtCLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLFNBQVMsR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsVUFBVSxJQUFJLENBQUMsRUFBRSx3QkFBd0IsaUJBQWlCLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQTtnQkFDaEksY0FBYyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDbkQsQ0FBQztZQUNELE1BQU0sQ0FBQyxjQUFjLENBQUE7UUFDdkIsQ0FBQztLQUFBO0lBRUssT0FBTyxDQUFFLFNBQVMsRUFBRSxRQUFnQixFQUFFLE9BQU8sR0FBRyxFQUFFOztZQUN0RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUE7WUFDbkQsd0JBQXdCO1lBQ3hCLDBCQUEwQjtZQUMxQixJQUFJLFVBQVUsR0FBOEM7Z0JBQzFELE1BQU0sRUFBRSxTQUFTO2dCQUNqQixPQUFPLEVBQUU7b0JBQ1AsZUFBZSxFQUFFLElBQUksQ0FBQyxXQUFXO29CQUNqQyxjQUFjLEVBQUUsa0JBQWtCO2lCQUNuQztnQkFDRCxXQUFXLEVBQUUsVUFBVTthQUN4QixDQUFBO1lBQ0QsSUFBSSxZQUFZLEdBQUcsV0FBVyxJQUFJLENBQUMsV0FBVyx5QkFBeUIsUUFBUSxFQUFFLENBQUE7WUFDakYsSUFBSSxRQUFhLENBQUE7WUFDakIsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLFFBQVEsR0FBRyxNQUFNLE9BQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQTtnQkFDdkUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUE7WUFDdEIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBQyxHQUFHLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFO3dCQUM3RCxlQUFlLEVBQUUsSUFBSSxDQUFDLFdBQVc7d0JBQ2pDLGNBQWMsRUFBRSxrQkFBa0I7cUJBQ25DLEVBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxJQUFJO29CQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQ25CLENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQztRQUNILENBQUM7S0FBQTtDQUNGO0FBL0ZELHNDQStGQyJ9